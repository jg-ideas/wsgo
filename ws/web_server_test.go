package ws

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPostProcessController(t *testing.T) {
	type args struct {
		next Controller
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Should handle error",
			args: args{
				next: func(w http.ResponseWriter, r *http.Request) (model interface{}, err error) {
					return nil, &ControllerError{
						StatusCode:   http.StatusBadRequest,
						ErrorMessage: "an error message",
					}
				},
			},
			want: http.StatusBadRequest,
		},
		{
			name: "Should handle normal status ok",
			args: args{
				next: func(w http.ResponseWriter, r *http.Request) (model interface{}, err error) {
					return &serverStatus{}, nil
				},
			},
			want: http.StatusOK,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := httptest.NewRequest("GET", "http://example.com/foo", nil)
			w := httptest.NewRecorder()
			PostProcessController(tt.args.next)(w, req)

			response := w.Result()
			if response.StatusCode != tt.want {
				t.Errorf("Expected [%v], got [%v]", tt.want, response.StatusCode)
			}
		})
	}
}
