package ws

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

const contentTypeHeader = "Content-Type"
const defaultContentType = "application/json"

// ServerSettings to implement on your own web service
type ServerSettings interface {
	Host() string
	Port() string
	Base() string
}

// ConfigurableWebServer is a representation of a web server
type ConfigurableWebServer struct {
	httpServer  *http.Server
	contextPath string
	router      *mux.Router
}

// Controller processes requests and returns a model
type Controller func(w http.ResponseWriter, r *http.Request) (model interface{}, err error)

// ControllerError for
type ControllerError struct {
	ErrorMessage string
	StatusCode   int
}

func (e *ControllerError) Error() string {
	return fmt.Sprintf("Error %s", e.ErrorMessage)
}

type serverStatus struct {
	Status  string `json:"status"`
	Version string `json:"version"`
}

// NewWebServer instantiates a new web server
func NewWebServer(serverSettings ServerSettings) *ConfigurableWebServer {

	if serverSettings.Host() == "" {
		panic("Host was not specified on the settings")
	}

	if serverSettings.Port() == "" {
		panic("Port was not specified on the settings")
	}

	base := serverSettings.Base()
	if base != "" {
		base = "/" + base
	}

	theServer := &ConfigurableWebServer{
		httpServer: &http.Server{
			Addr: net.JoinHostPort(serverSettings.Host(), serverSettings.Port()),
		},
		contextPath: base,
	}

	theServer.initRouteHandlers()
	return theServer
}

// Start starts serving on the configured host and port
func (configurableWebServer *ConfigurableWebServer) Start() error {
	return configurableWebServer.httpServer.ListenAndServe()
}

// Stop stops the web server
func (configurableWebServer *ConfigurableWebServer) Stop() error {
	timeout := 10 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	return configurableWebServer.httpServer.Shutdown(ctx)
}

// PostProcessController does post processing for a controller before returnign a response
func PostProcessController(next Controller) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		payload, err := next(w, r)
		if err != nil {
			if err, ok := err.(*ControllerError); ok {
				RespondError(w, err.StatusCode, err.Error())
				return
			}
			log.Print(err)
			RespondError(w, http.StatusInternalServerError, "An unknown error occurred")
			return
		}
		RespondJSON(w, http.StatusOK, payload)
	}
}

func (configurableWebServer *ConfigurableWebServer) initRouteHandlers() {
	router := mux.NewRouter()

	router.HandleFunc(configurableWebServer.contextPath+"/heartbeat", PostProcessController(configurableWebServer.heartbeatController))
	router.Use(configurableWebServer.LoggingMiddleware)
	router.Use(configurableWebServer.ContentTypeMiddleware)
	configurableWebServer.httpServer.Handler = router
	configurableWebServer.router = router
}

// AddRouterHandleFunc adds a controller for handling requests/resonse to the router.
// Returns an error if contextPath has an incorrect format.
func (configurableWebServer *ConfigurableWebServer) AddRouterHandleFunc(path string, f Controller, methods ...string) error {
	router := configurableWebServer.router
	if strings.HasPrefix(path, "/") {
		newPath := configurableWebServer.contextPath + path
		router.HandleFunc(newPath, PostProcessController(f)).Methods(methods...)
		return nil
	}
	return errors.New("Invalid format for contextPath")
}

func (configurableWebServer *ConfigurableWebServer) heartbeatController(w http.ResponseWriter, r *http.Request) (model interface{}, err error) {
	return serverStatus{
		Status:  "OK",
		Version: "0.1",
	}, nil
}

// LoggingMiddleware used for debugging
func (configurableWebServer *ConfigurableWebServer) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Method [%v], URI [%v], From [%v] [%v] \n", r.Method, r.RequestURI, r.RemoteAddr, r.UserAgent())
		next.ServeHTTP(w, r)
	})
}

// ContentTypeMiddleware sets/adds header values
func (configurableWebServer *ConfigurableWebServer) ContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		next.ServeHTTP(w, r)
	})
}

// RespondJSON makes the response with payload as json format
func RespondJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(status)
	w.Write([]byte(response))
}

// RespondError makes the error response with payload as json format
func RespondError(w http.ResponseWriter, code int, message string) {
	RespondJSON(w, code, map[string]string{"error": message})
}
